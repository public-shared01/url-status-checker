package internal

import (
	"crypto/tls"
	"errors"
	"fmt"
	"io"
	"net"
	"net/http"
	"strings"
	"time"
)

type ResponseStatus struct {
	URL          string
	HTTPCode     int
	ResponseTime float64
	TLSExpiry    int
	IsItDown     int
}

func CheckStatus(url string) (*ResponseStatus, error) {
	if !strings.HasPrefix(url, "https://") {
		url = "https://" + url
	}

	start := time.Now()
	client := &http.Client{
		Timeout: 10 * time.Second,
	}

	resp, err := client.Get(url)
	if err != nil {
		var netErr net.Error
		if errors.As(err, &netErr) {
			if netErr.Timeout() {
				return nil, fmt.Errorf("timeout error: %w", err)
			}
			if netErr.Temporary() {
				return nil, fmt.Errorf("temporary network error: %w", err)
			}
		}
		return nil, fmt.Errorf("network error: %w", err)
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			fmt.Println("Error: ", err)
		}
	}(resp.Body)

	duration := time.Since(start)
	tlsExpiryDate, err := checkSSLCert(url)
	boolToInt := func(b bool) int {
		if b {
			return 0
		}
		return 1
	}

	status := &ResponseStatus{
		URL:          url,
		HTTPCode:     resp.StatusCode,
		ResponseTime: duration.Seconds(),
		TLSExpiry:    tlsExpiryDate,
		IsItDown:     boolToInt(resp.StatusCode == http.StatusOK),
	}
	return status, nil
}
func checkSSLCert(url string) (int, error) {
	conn, err := tls.Dial("tcp", strings.TrimPrefix(url, "https://")+":443", &tls.Config{})
	if err != nil {
		return 0, fmt.Errorf("connection error: %w", err)
	}
	defer func() {
		err := conn.Close()
		if err != nil {
			fmt.Println("Conn close error: ", err)
		}
	}()

	certs := conn.ConnectionState().PeerCertificates
	if len(certs) == 0 {
		return 0, fmt.Errorf("no certificates found")
	}

	cert := certs[0]
	if time.Now().Before(cert.NotAfter) {
		daysLeft := cert.NotAfter.Sub(time.Now()).Hours() / 24
		return int(daysLeft), nil
	}
	return 0, nil
}
