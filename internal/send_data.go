package internal

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/DataDog/datadog-api-client-go/v2/api/datadog"
	"github.com/DataDog/datadog-api-client-go/v2/api/datadogV2"
	"os"
	"time"
)

func MetricsSubmit(status *ResponseStatus) (*datadogV2.MetricPayload, error) {

	metric := datadogV2.MetricPayload{
		Series: []datadogV2.MetricSeries{
			{
				Metric: "endpoint.response_time",
				Type:   datadogV2.METRICINTAKETYPE_GAUGE.Ptr(),
				Points: []datadogV2.MetricPoint{
					{
						Timestamp: datadog.PtrInt64(time.Now().Unix()),
						Value:     datadog.PtrFloat64(status.ResponseTime),
					},
				},
				Resources: []datadogV2.MetricResource{
					{
						Name: datadog.PtrString(status.URL),
						Type: datadog.PtrString("url"),
					},
				},
			},
			{
				Metric: "endpoint.http_code",
				Type:   datadogV2.METRICINTAKETYPE_GAUGE.Ptr(),
				Points: []datadogV2.MetricPoint{
					{
						Timestamp: datadog.PtrInt64(time.Now().Unix()),
						Value:     datadog.PtrFloat64(float64(status.HTTPCode)),
					},
				},
				Resources: []datadogV2.MetricResource{
					{
						Name: datadog.PtrString(status.URL),
						Type: datadog.PtrString("url"),
					},
				},
			},
			{
				Metric: "endpoint.tls_days_left",
				Type:   datadogV2.METRICINTAKETYPE_COUNT.Ptr(),
				Points: []datadogV2.MetricPoint{
					{
						Timestamp: datadog.PtrInt64(time.Now().Unix()),
						Value:     datadog.PtrFloat64(float64(status.TLSExpiry)),
					},
				},
				Resources: []datadogV2.MetricResource{
					{
						Name: datadog.PtrString(status.URL),
						Type: datadog.PtrString("url"),
					},
				},
			},
			{
				Metric: "endpoint.live",
				Type:   datadogV2.METRICINTAKETYPE_COUNT.Ptr(),
				Points: []datadogV2.MetricPoint{
					{
						Timestamp: datadog.PtrInt64(time.Now().Unix()),
						Value:     datadog.PtrFloat64(float64(status.IsItDown)),
					},
				},
				Resources: []datadogV2.MetricResource{
					{
						Name: datadog.PtrString(status.URL),
						Type: datadog.PtrString("url"),
					},
				},
			},
		},
	}

	return &metric, nil
}

func SendMetric(ctx context.Context, api *datadogV2.MetricsApi, status *ResponseStatus) (*[]byte, error) {
	metric, err := MetricsSubmit(status)
	if err != nil {
		return nil, err
	}

	resp, r, err := api.SubmitMetrics(ctx, *metric, *datadogV2.NewSubmitMetricsOptionalParameters())
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error when calling `MetricsApi.SubmitMetrics`: %v\n", err)
		fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
		return nil, err
	}

	responseContent, err := json.MarshalIndent(resp, "", "  ")
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error parsing JSON: %v\n", err)
		return nil, err
	}

	return &responseContent, nil
}
