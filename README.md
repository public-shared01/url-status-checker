URL-status-checker
---

This service took data from any endpoints in the config JSON file 'urls.json'
and make checks for availability of the endpoints. The service will submit a custom metric to the Datadog API with the status of the endpoints.

## Requirements
Environment variables:
- CHECK_INTERVAL: Time in seconds to check the endpoints
```CHECK_INTERVAL=<time in seconds>```
- DD_API_KEY: Datadog API key
```DD_API_KEY=<your api key>```
- DD_SITE: Datadog endpoint
```DD_SITE=<your datadog endpoint>```
- FILE_PATH: Path to the config file
```FILE_PATH=<./config/urls.json>```

## Custom metric schema
```
{"URL":"https://example.com","HTTPCode":200,"ResponseTime":0.394634657,"SSLExpiry":"2024-10-18T22:25:54Z"}
```
