package config

import (
	"encoding/json"
	"log"
	"os"
	"strconv"
	"time"
)

type Endpoint struct {
	Url string `json:"url"`
}

type Config struct {
	Endpoints     []Endpoint
	CheckInterval time.Duration
	DdApiKey      string
	DdSite        string
}

func fetchEnvironmentVariable(key string) string {
	value, defined := os.LookupEnv(key)

	if !defined {
		log.Fatalf("environment Variable %s is required but not defined", key)
	}

	return value
}

func LoadConfig() *Config {
	jsonData, err := os.ReadFile(fetchEnvironmentVariable("FILE_PATH"))
	if err != nil {
		log.Fatalf("Error reading file: %v", err)
		return nil
	}

	var arr []Endpoint
	err = json.Unmarshal(jsonData, &arr)
	if err != nil {
		log.Fatalf("Error parsing JSON: %v", err)
		return nil
	}

	ddApiKey := fetchEnvironmentVariable("DD_API_KEY")
	ddSite := fetchEnvironmentVariable("DD_SITE")
	varCheckInt := fetchEnvironmentVariable("CHECK_INTERVAL")
	CheckInterval, _ := strconv.ParseInt(varCheckInt, 10, 64)
	if err != nil {
		log.Fatalf("Invalid CHECK_INTERVAL value: %v", err)
	}

	return &Config{
		Endpoints:     arr,
		CheckInterval: time.Duration(CheckInterval),
		DdApiKey:      ddApiKey,
		DdSite:        ddSite,
	}
}
