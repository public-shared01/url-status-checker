package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/DataDog/datadog-api-client-go/v2/api/datadog"
	"github.com/DataDog/datadog-api-client-go/v2/api/datadogV2"
	"os"
	"os/signal"
	"syscall"
	"time"
	"url-status-checker/config"
	"url-status-checker/internal"
)

func main() {
	cfg := config.LoadConfig()

	signalCtx, err := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %v\n", err)
	}

	ctx := context.WithValue(signalCtx, datadog.ContextAPIKeys, map[string]datadog.APIKey{
		"apiKeyAuth": {
			Key: cfg.DdApiKey,
		},
	})
	configuration := datadog.NewConfiguration()
	configuration.Host = cfg.DdSite
	apiClient := datadog.NewAPIClient(configuration)
	api := datadogV2.NewMetricsApi(apiClient)

	for {
		select {
		case <-ctx.Done():
			fmt.Println("Received signal <SIGTERM>. Shutting down...")
			return
		default:
			for _, endpoint := range cfg.Endpoints {
				status, err := internal.CheckStatus(endpoint.Url)
				if err != nil {
					fmt.Println(err)
					continue
				}

				content, err := json.Marshal(status)
				if err != nil {
					fmt.Fprintf(os.Stderr, "Error parsing JSON): %v\n", err)
					continue
				}
				println(string(content))
				internal.MetricsSubmit(status)
				internal.SendMetric(ctx, api, status)

			}
			time.Sleep(cfg.CheckInterval * time.Second)
		}
	}
}
